package v1

import (
	"encoding/json"
	"fmt"
	"net/http"

	// "strings"
	"github.com/golang/protobuf/jsonpb"

	"github.com/gin-gonic/gin"
	"gitlab.com/todos/todos/api/models"
	"gitlab.com/todos/todos/storage/repo"
)

// CreateTodo ...
// @Security ApiKeyAuth
// @Summary Create Todo
// @Description Create Todo - API for creating new todo for user
// @Tags todos
// @Accept  json
// @Produce  json
// @Param contact body models.Todos true "todos"
// @Success 201
// @Failure 401
// @Router /v1/todos/create/ [post]
func (h *HandlerV1) CreateTodo(c *gin.Context) {
	var (
		model  models.Todos
		result map[string]interface{}
	)
	claims := GetClaims(h, c)

	err := c.ShouldBindJSON(&model)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorBadRequest,
				Message: "Error while binding json",
			},
		})
		return
	}

	url := "http://localhost:1235/v1/user/profile/" + claims["sub"].(string) + "/"

	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorBadRequest,
				Message: "Error while getting user info, users service unavailable",
			},
		})
		return
	}

	req.Header.Set("Authorization", c.GetHeader("Authorization"))

	res, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorBadRequest,
				Message: "Error while getting user info, users service unavailable",
			},
		})
		return
	}

	defer res.Body.Close()

	json.NewDecoder(res.Body).Decode(&result)

	if result["username"] != nil {
		err = h.storage.Todo().Create(&repo.Todo{
			Heading:     model.Heading,
			Description: model.Description,
			User: repo.User{
				ID: claims["sub"].(string),
			},
			DeadlineDate: model.DeadlineDate,
		})

		c.JSON(http.StatusCreated, fmt.Sprintf("Todo %s successfully created!", model.Heading))
	} else {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorBadRequest,
				Message: "Something went wrong",
			},
		})
		return
	}
}

// Update ...
// @Security ApiKeyAuth
// @Summary Update todo
// @Description Update todo updates todo's request
// @Tags todos
// @Accept json
// @Produce json
// @Param delete body models.UpdateData true "update"
// @Success 201
// @Failure 401
// @Router /v1/todos/update/ [put]
func (h *HandlerV1) Update(c *gin.Context) {
	var (
		body        models.UpdateData
		jspbMarshal jsonpb.Marshaler
	)
	jspbMarshal.OrigName = true

	GetClaims(h, c)

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorBadRequest,
				Message: "Error while binding json",
			},
		})
	}

	err = h.storage.Todo().Update(&repo.Todo{
		ID:           body.ID,
		Heading:      body.Heading,
		Description:  body.Description,
		DeadlineDate: body.DeadlineDate,
	})
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorBadRequest,
				Message: "Error while updating todo",
			},
		})
		return
	}

	c.Status(http.StatusCreated)
}

// GetTodo ...
// @Security ApiKeyAuth
// @Summary Get GetTodo
// @Description Get Todo API returns a todo's info.
// @Param id path string true "Todo id"
// @Tags todos
// @Accept  json
// @Produce  json
// @Success 201 {object} models.TodosResponse
// @Failure 401
// @Router /v1/todos/info/{id}/ [GET]
func (h *HandlerV1) GetTodo(c *gin.Context) {
	var (
		ID     string
		err    error
		result map[string]interface{}
	)
	ID = c.Param("id")

	claims := GetClaims(h, c)
	url := "http://localhost:1235/v1/user/profile/" + claims["sub"].(string) + "/"

	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorBadRequest,
				Message: "Error while getting user info, users service unavailable",
			},
		})
		return
	}
	req.Header.Set("Authorization", c.GetHeader("Authorization"))

	res, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorBadRequest,
				Message: "Error while getting user info, users service unavailable",
			},
		})
		return
	}
	defer res.Body.Close()

	json.NewDecoder(res.Body).Decode(&result)
	resp, err := h.storage.Todo().GetTodo(ID)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorBadRequest,
				Message: "Error while getting todo",
			},
		})
		return
	}
	resp.User.Username = result["username"].(string)

	c.JSON(http.StatusCreated, resp)
}

// GetUserTodos ...
// @Security ApiKeyAuth
// @Summary Get User's Todos
// @Description Get User's Todos API returns a todos info of user
// @Tags todos
// @Accept  json
// @Produce  json
// @Success 201
// @Failure 401
// @Router /v1/todos/user/ [GET]
func (h *HandlerV1) GetUserTodos(c *gin.Context) {
	var (
		err    error
		result map[string]interface{}
	)

	claims := GetClaims(h, c)

	resp, err := h.storage.Todo().GetUserTodos(claims["sub"].(string))
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorBadRequest,
				Message: "Error while getting all user's todos",
			},
		})
		return
	}

	for _, user := range resp {
		url := "http://localhost:1235/v1/user/profile/" + claims["sub"].(string) + "/"

		client := &http.Client{}
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			c.JSON(http.StatusUnauthorized, models.ResponseError{
				Error: models.ServerError{
					Status:  ErrorBadRequest,
					Message: "Error while getting user info, users service unavailable",
				},
			})
			return
		}
		req.Header.Set("Authorization", c.GetHeader("Authorization"))

		res, err := client.Do(req)
		if err != nil {
			c.JSON(http.StatusUnauthorized, models.ResponseError{
				Error: models.ServerError{
					Status:  ErrorBadRequest,
					Message: "Error while getting user info, users service unavailable",
				},
			})
			return
		}
		defer res.Body.Close()

		json.NewDecoder(res.Body).Decode(&result)
		user.User.Username = result["username"].(string)
	}

	c.JSON(http.StatusCreated, resp)
}

// GetUserTodos ...
// @Security ApiKeyAuth
// @Summary Get User's Todos by deadline
// @Description Get User's Todos API returns todos which deadline's date should be done by given date
// @Param request body models.DeadlineDate true "date"
// @Tags todos
// @Accept  json
// @Produce  json
// @Success 201
// @Failure 401
// @Router /v1/todos/deadline/ [POST]
func (h *HandlerV1) GetTodosByDeadline(c *gin.Context) {
	var (
		err    error
		result map[string]interface{}
		body   models.DeadlineDate
	)

	claims := GetClaims(h, c)

	err = c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorBadRequest,
				Message: "Error while binding json",
			},
		})
		return
	}

	resp, err := h.storage.Todo().GetTodosByDeadline(body.Date)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorBadRequest,
				Message: "Error while getting all user's todos by date",
			},
		})
		return
	}

	for _, user := range resp {
		url := "http://localhost:1235/v1/user/profile/" + claims["sub"].(string) + "/"

		client := &http.Client{}
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			c.JSON(http.StatusUnauthorized, models.ResponseError{
				Error: models.ServerError{
					Status:  ErrorBadRequest,
					Message: "Error while getting user info, users service unavailable",
				},
			})
			return
		}
		req.Header.Set("Authorization", c.GetHeader("Authorization"))

		res, err := client.Do(req)
		if err != nil {
			c.JSON(http.StatusUnauthorized, models.ResponseError{
				Error: models.ServerError{
					Status:  ErrorBadRequest,
					Message: "Error while getting user info, users service unavailable",
				},
			})
			return
		}
		defer res.Body.Close()

		json.NewDecoder(res.Body).Decode(&result)
		user.User.Username = result["username"].(string)
	}

	c.JSON(http.StatusCreated, resp)
}
