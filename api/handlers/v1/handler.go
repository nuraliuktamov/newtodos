package v1

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	jwtg "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/todos/todos/api/models"
	"gitlab.com/todos/todos/api/token"
	"gitlab.com/todos/todos/pkg/logger"
	"gitlab.com/todos/todos/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// HandlerV1 ..																																																																																																																																																																																										.
type HandlerV1 struct {
	storage    storage.StorageI
	log        logger.Logger
	jwtHandler token.JWTHandler
}

// New ...
func New(Storage storage.StorageI, log logger.Logger, jwtHandler token.JWTHandler) *HandlerV1 {
	return &HandlerV1{
		storage:    Storage,
		log:        log,
		jwtHandler: jwtHandler,
	}
}

const (
	//ErrorCodeInvalidURL ...
	ErrorCodeInvalidURL = "INVALID_URL"
	//ErrorCodeInvalidJSON ...
	ErrorCodeInvalidJSON = "INVALID_JSON"
	//ErrorCodeInvalidParams ...
	ErrorCodeInvalidParams = "INVALID_PARAMS"
	//ErrorCodeInternal ...
	ErrorCodeInternal = "INTERNAL"
	//ErrorCodeUnauthorized ...
	ErrorCodeUnauthorized = "UNAUTHORIZED"
	//ErrorCodeAlreadyExists ...
	ErrorCodeAlreadyExists = "ALREADY_EXISTS"
	//ErrorCodeNotFound ...
	ErrorCodeNotFound = "NOT_FOUND"
	//ErrorCodeInvalidCode ...
	ErrorCodeInvalidCode = "INVALID_CODE"
	//ErrorBadRequest ...
	ErrorBadRequest = "BAD_REQUEST"
	//ErrorCodeForbidden ...
	ErrorCodeForbidden = "FORBIDDEN"
	//ErrorCodeNotApproved ...
	ErrorCodeNotApproved = "NOT_APPROVED"
	//ErrorCodeWrongClub ...
	ErrorCodeWrongClub = "WRONG_CLUB"
	//ErrorCodePasswordsNotEqual ...
	ErrorCodePasswordsNotEqual = "PASSWORDS_NOT_EQUAL"
	// ErrorExpectationFailed ...
	ErrorExpectationFailed = "EXPECTATION_FAILED"
	// ErrorUpgradeRequired ...
	ErrorUpgradeRequired = "UPGRADE_REQUIRED"
	// ErrorInvalidCredentials ...
	ErrorInvalidCredentials = "INVALID_CREDENTIALS"
)

var (
	// MySigningKey ...
	MySigningKey = []byte("secretphrase")
	// NewSigningKey ...
	NewSigningKey = []byte("FfLbN7pIEYe8@!EqrttOLiwa(H8)7Ddo")
)

// ParsePageQueryParam ...
func ParsePageQueryParam(c *gin.Context) (int, error) {
	page, err := strconv.Atoi(c.DefaultQuery("page", "1"))
	if err != nil {
		return 0, err
	}
	if page < 0 {
		return 0, errors.New("page must be an positive integer")
	}
	if page == 0 {
		return 1, nil
	}
	return page, nil
}

// ParseLimitQueryParam ...
func ParseLimitQueryParam(c *gin.Context) (int, error) {
	limit, err := strconv.Atoi(c.DefaultQuery("limit", "10"))
	if err != nil {
		return 0, err
	}
	if limit < 0 {
		return 0, errors.New("page_size must be an positive integer")
	}
	if limit == 0 {
		return 10, nil
	}
	return limit, nil
}

// ParsePageSizeQueryParam ...
func ParsePageSizeQueryParam(c *gin.Context) (int, error) {
	pageSize, err := strconv.Atoi(c.DefaultQuery("page_size", "10"))
	if err != nil {
		return 0, err
	}
	if pageSize < 0 {
		return 0, errors.New("page_size must be an positive integer")
	}
	return pageSize, nil
}

func handleGRPCErr(c *gin.Context, l logger.Logger, err error) bool {
	if err == nil {
		return false
	}
	st, ok := status.FromError(err)
	var errI interface{} = models.InternalServerError{
		Status:  ErrorCodeInternal,
		Message: "Internal Server Error",
	}
	httpCode := http.StatusInternalServerError
	if ok && st.Code() == codes.InvalidArgument {
		httpCode = http.StatusBadRequest
		errI = ErrorBadRequest
	}
	c.JSON(httpCode, models.ResponseError{
		Error: errI,
	})
	if ok {
		l.Error(fmt.Sprintf("code=%d message=%s", st.Code(), st.Message()), logger.Error(err))
	}
	return true
}

func handleGrpcErrWithMessage(c *gin.Context, l logger.Logger, err error, message string, args ...interface{}) bool {
	st, ok := status.FromError(err)
	if !ok || st.Code() == codes.Internal {

		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorCodeInternal,
				Message: st.Message(),
			},
		})
		l.Error(message, logger.Error(err), logger.Any("req", args))
		return true
	} else if st.Code() == codes.NotFound {
		c.JSON(http.StatusNotFound, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorCodeNotFound,
				Message: st.Message(),
			},
		})
		l.Error(message+", not found", logger.Error(err), logger.Any("req", args))
		return true
	} else if st.Code() == codes.Unavailable {
		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorCodeInternal,
				Message: "Internal Server Error",
			},
		})
		l.Error(message+", service unavailable", logger.Error(err), logger.Any("req", args))
		return true
	} else if st.Code() == codes.AlreadyExists {
		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorCodeAlreadyExists,
				Message: st.Message(),
			},
		})
		l.Error(message+", already exists", logger.Error(err), logger.Any("req", args))
		return true
	} else if st.Code() == codes.InvalidArgument {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorBadRequest,
				Message: st.Message(),
			},
		})
		l.Error(message+", invalid field", logger.Error(err), logger.Any("req", args))
		return true
	} else if st.Code() == codes.DataLoss {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorBadRequest,
				Message: st.Message(),
			},
		})
		l.Error(message+", invalid field", logger.Error(err), logger.Any("req", args))
		return true
	} else if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorBadRequest,
				Message: st.Message(),
			},
		})
		l.Error(message+", unknown error", logger.Error(err), logger.Any("req", args))
		return true
	}

	return false
}

func handleInternalWithMessage(c *gin.Context, err error, message string, args ...interface{}) bool {
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorCodeInternal,
				Message: "Internal Server Error",
			},
		})
	}

	return false
}

func handleBadRequestErrWithMessage(c *gin.Context, l logger.Logger, err error, message string, args ...interface{}) bool {
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorCodeInvalidCode,
				Message: "Incorrect data supplied",
			},
		})
		l.Error(message, logger.Error(err), logger.Any("req", args))
		return true
	}
	return false
}

// GetClaims ...
func GetClaims(h *HandlerV1, c *gin.Context) jwtg.MapClaims {
	var (
		ErrUnauthorized = errors.New("unauthorized")
		authorization   models.GetProfileByJwtRequestModel
		claims          jwtg.MapClaims
		err             error
	)

	authorization.Token = c.GetHeader("Authorization")
	if c.Request.Header.Get("Authorization") == "" {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.InternalServerError{
				Status:  ErrorCodeUnauthorized,
				Message: "Unauthorized request",
			},
		})
		h.log.Error("Unauthorized request: ", logger.Error(ErrUnauthorized))
		return nil
	}

	h.jwtHandler.Token = authorization.Token
	claims, err = h.jwtHandler.ExtractClaims()
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.InternalServerError{
				Status:  ErrorCodeUnauthorized,
				Message: "Unauthorized request",
			},
		})
		h.log.Error("Unauthorized request: ", logger.Error(err))
		return nil
	}
	return claims
}
