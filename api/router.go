package api

import (
	casbin "github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/todos/todos/api/docs"
	v1 "gitlab.com/todos/todos/api/handlers/v1"
	"gitlab.com/todos/todos/api/middleware"
	"gitlab.com/todos/todos/api/token"
	"gitlab.com/todos/todos/config"
	"gitlab.com/todos/todos/storage"

	"gitlab.com/todos/todos/pkg/logger"
)

// Config ...
type Config struct {
	Storage        storage.StorageI
	Logger         logger.Logger
	Cfg            config.Config
	CasbinEnforcer *casbin.Enforcer
}

// New is a constructor for gin.Engine
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func New(cfg Config) *gin.Engine {
	r := gin.New()

	r.Use(gin.Logger())

	jwtHandler := token.JWTHandler{
		SigninKey: cfg.Cfg.SigninKey,
		Log:       cfg.Logger,
	}

	r.Use(gin.Recovery())
	r.Use(middleware.NewAuthorizer(cfg.CasbinEnforcer, jwtHandler, cfg.Cfg))

	// HandlerV1 should be replaced with handlerV1
	HandlerV1 := v1.New(cfg.Storage, cfg.Logger, jwtHandler)

	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	groupV1 := r.Group("/v1")
	// todos
	groupV1.POST("/todos/create/", HandlerV1.CreateTodo)
	groupV1.PUT("/todos/update/", HandlerV1.Update)
	groupV1.GET("/todos/info/:id/", HandlerV1.GetTodo)
	groupV1.GET("/todos/user/", HandlerV1.GetUserTodos)
	groupV1.POST("/todos/deadline/", HandlerV1.GetTodosByDeadline)

	return r
}
