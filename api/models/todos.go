package models

//SendCodeResponse ...
type SendCodeResponse struct {
	ID string `json:"id"`
}

type Todos struct {
	Heading      string `json:"heading"`
	Description  string `json:"description"`
	UserID       string `json:"user_id"`
	DeadlineDate string `json:"deadline_date"`
}

type TodosResponse struct {
	ID           string `json:"id"`
	Heading      string `json:"heading"`
	Description  string `json:"description"`
	UserID       string `json:"user_id"`
	DeadlineDate string `json:"deadline_date"`
}

type UpdateData struct {
	ID           string `json:"id"`
	Heading      string `json:"heading"`
	Description  string `json:"description"`
	DeadlineDate string `json:"deadline_date"`
}

//User ...
type User struct {
	ID       string `json:"id,omitempty"`
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
}

type AllTodos struct {
	Data []TodosResponse `json:"data,omitempty"`
}

type DeadlineDate struct {
	Date string `json:"date,omitempty"`
}
