package models

// ResponseSuccess ...
type ResponseSuccess struct {
	Metadata interface{}
	Data     interface{}
}

// ResponseError ...
type ResponseError struct {
	Error interface{} `json:"error"`
}

// Error ...
type Error struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

// ServerError ...
type ServerError struct {
	Status  string
	Message string
}

// ValidationError ...
type ValidationError struct {
	Code        string
	Message     string
	UserMessage string
}

//InternalServerError ...
type InternalServerError struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

// StandardErrorModel ...
type StandardErrorModel struct {
	Error Error `json:"error"`
}
