package storage

import (
	"github.com/jmoiron/sqlx"

	"gitlab.com/todos/todos/storage/postgres"
	"gitlab.com/todos/todos/storage/repo"
)

// StorageI ...
type StorageI interface {
	Todo() repo.TodoStorageI
}

type storagePg struct {
	db       *sqlx.DB
	todoRepo repo.TodoStorageI
}

// NewStoragePg ...
func NewStoragePg(db *sqlx.DB) StorageI {
	return &storagePg{
		db:       db,
		todoRepo: postgres.NewTodoRepo(db),
	}
}

func (s storagePg) Todo() repo.TodoStorageI {
	return s.todoRepo
}
