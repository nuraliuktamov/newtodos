package repo

//User ...
type User struct {
	ID       string `json:"id,omitempty"`
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
}

type Todo struct {
	ID           string `json:"id"`
	Heading      string `json:"heading"`
	Description  string `json:"description"`
	User         User   `json:"user"`
	DeadlineDate string `json:"deadline_date"`
}

//UserStorageI ...
type TodoStorageI interface {
	Create(*Todo) error
	Update(*Todo) error
	Delete(ID string) error
	GetTodo(ID string) (*Todo, error)
	GetUserTodos(userID string) (users []*Todo, err error)
	GetTodosByDeadline(deadlineDate string) (users []*Todo, err error)
}
