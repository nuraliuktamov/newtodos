package postgres

import (
	"github.com/jmoiron/sqlx"

	"github.com/google/uuid"
	_ "github.com/lib/pq" // for db driver
	"gitlab.com/todos/todos/storage/repo"
)

type todoRepo struct {
	db *sqlx.DB
}

// NewUserRepo ...
func NewTodoRepo(db *sqlx.DB) repo.TodoStorageI {
	return &todoRepo{db: db}
}

func (cm *todoRepo) Create(todo *repo.Todo) error {
	var (
		err error
	)
	id, err := uuid.NewRandom()
	if err != nil {
		return err
	}

	insertNew :=
		`INSERT INTO
		todos
		(
			id,
			user_id,
			description,
			heading,
			deadline_date
		)
			VALUES  
			($1, $2, $3, $4, $5)`
	_, err = cm.db.Exec(
		insertNew,
		id.String(),
		todo.User.ID,
		todo.Description,
		todo.Heading,
		todo.DeadlineDate,
	)
	if err != nil {
		return err
	}
	return nil
}

func (cm *todoRepo) Update(todo *repo.Todo) error {
	updateClient := `
	UPDATE todos 
	SET
		heading = $1,
		description = $2,
		deadline_date = $3
	WHERE id = $4
	`
	_, err := cm.db.Exec(updateClient,
		todo.Heading,
		todo.Description,
		todo.DeadlineDate,
		todo.ID,
	)
	if err != nil {
		return err
	}
	return nil
}

func (cm *todoRepo) GetTodo(ID string) (*repo.Todo, error) {
	todo := repo.Todo{}

	row := cm.db.QueryRow(`SELECT id, user_id, heading, description, deadline_date from todos WHERE id = $1`, ID)
	err := row.Scan(
		&todo.ID,
		&todo.User.ID,
		&todo.Heading,
		&todo.Description,
		&todo.DeadlineDate,
	)
	if err != nil {
		return nil, err
	}

	return &todo, nil
}

func (cm *todoRepo) GetUserTodos(userID string) ([]*repo.Todo, error) {

	todos := make([]*repo.Todo, 0)

	rows, err := cm.db.Queryx(`SELECT id, user_id, heading, description, deadline_date from todos WHERE user_id = $1 ORDER BY deadline_date`, userID)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var todo repo.Todo
		if err := rows.Scan(
			&todo.ID,
			&todo.User.ID,
			&todo.Heading,
			&todo.Description,
			&todo.DeadlineDate,
		); err != nil {
			return nil, err
		}

		todos = append(todos, &todo)
	}

	return todos, nil
}

func (cm *todoRepo) GetTodosByDeadline(deadlineDate string) ([]*repo.Todo, error) {

	todos := make([]*repo.Todo, 0)

	rows, err := cm.db.Queryx(`SELECT id, user_id, heading, description, deadline_date from todos WHERE deadline_date > $1`, deadlineDate)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var todo repo.Todo
		if err := rows.Scan(
			&todo.ID,
			&todo.User.ID,
			&todo.Heading,
			&todo.Description,
			&todo.DeadlineDate,
		); err != nil {
			return nil, err
		}

		todos = append(todos, &todo)
	}

	return todos, nil
}

func (cm *todoRepo) Delete(ID string) error {
	_, err := cm.db.Exec(`DELETE FROM todos WHERE id = $1`, ID)
	if err != nil {
		return err
	}

	return nil
}
